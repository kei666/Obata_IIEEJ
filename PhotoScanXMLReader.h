#ifndef PHOTOSCANXMLREADER_H
#define PHOTOSCANXMLREADER_H

// Std
#include <iostream>
#include <vector>
#include <list>
using namespace std;
// Boost
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
// GLM
//#include <glm/glm.hpp>
// OpenCV
#include "opencv2/opencv.hpp"

// ===================================== //
// ===== Data of AgiSoft PhotoScan ===== //
// ===================================== //
// ----- Intrinsics
struct PhotoScanSensorData
{
	int width, height;
	double fx, fy, cx, cy, skew, k1, k2, k3, p1, p2;
};
// ----- Cameras
struct PhotoScanCameras
{
	vector<PhotoScanSensorData> sensorData;	// Data of cameras (e.g. intrinsic parameters)
	//vector<glm::mat4> matrices;				// View matrix
	vector<cv::Mat> matrices;
	vector<string> imgNames;				// Name of input images
};

// ============================================================ //
// ===== Ref: http://boostjp.github.io/tips/xml.html#read ===== //
// ============================================================ //
void PhotoScanCameraXMLReader(const string path, PhotoScanCameras &data);

void PhotoScanCameraXMLWriter(const string path, cv::Mat iParam, cv::Mat distCoeffs, std::vector<cv::Mat> camPoses);

#endif