#include "DataManager.h"
#include "Method.h"

//--固定カメラ画像群，外部パラメータ群・内部パラメータ
vector< cv::Mat > imgSeq_stCam;			//--固定カメラ画像群
vector< cv::Mat > extParamSeq_stCam;	//--外部パラメータ群
cv::Mat intParam_stCam;					//--内部パラメータ
cv::Mat distCoeffs_stCam;				//--ひずみ

//--移動カメラ画像列，内部パラメータ
vector< cv::Mat > imgSeq_movCam;		//--移動カメラ画像列
cv::Mat intParam_movCam;				//--内部パラメータ
cv::Mat distCoeffs_movCam;				//--ひずみ

//--
vector< cv::Mat > textureImages;
vector< vector< cv::Point3f > > textureImageCorners;

vector < cv::Point3f > reprojected3DPoints;
vector < cv::Point2f > groundTruth2DPoints;