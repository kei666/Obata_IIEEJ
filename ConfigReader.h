#ifndef CONFIGREADER
#define CONFIGREADER

#include <iostream>
using namespace std;
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/optional.hpp> 
using namespace boost::property_tree;

static const string CONFIG_DEFAULT_PATH = "./config_.ini";

class ConfigReader
{
private:
	ptree m_pt;

public:
	// ----- Constructor and destructor
	ConfigReader(string fileName = CONFIG_DEFAULT_PATH)
	{
		try {
			read_ini(fileName, this->m_pt);
		}
		catch (ptree_error &e) {
			cerr << ">> Error in ConfigReader: ptree_err## " << e.what() << endl;
			cerr << ">>>> Closing this program..." << endl;
			exit(EXIT_FAILURE);
		}
	}

	~ConfigReader()
	{

	}

	// ----- Functions
	template<typename T>
	void read(string dataName, T &data)
	{
		try {
			data = this->m_pt.get<T>(dataName);
		}
		catch (ptree_error &e) {
			cerr << ">> Error in ConfigReader: ptree_err## " << e.what() << endl;
		}
	}
};

#endif