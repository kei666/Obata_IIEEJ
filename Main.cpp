#include "Main.h"

int main(int argc, char** argv)
{
	DataManager* dm = new DataManager();
	Method* me = new Method();

	dm->readConfig();

	if (dm->mode == "MOV") {
		std::vector<std::vector<cv::Mat>> imgSeq_stCam_imgs;
		dm->readStCamImgSeq(imgSeq_stCam_imgs, extParamSeq_stCam);            //--�Œ�J�����摜�Q��ǂݍ����vector�Ɋi�[(�O���p����)
		dm->readMovCamImgSeq(imgSeq_movCam);                            //--�ړ��J�����摜���ǂݍ����vector�Ɋi�[

		dm->readIntrinsicsStCam(intParam_stCam,
								distCoeffs_stCam);        //--�Œ�J�����̓����p�����[�^��ǂݍ����cv::Mat�Ɋi�[
		dm->readIntrinsicsMovCam(intParam_movCam,
								 distCoeffs_movCam);    //--�ړ��J�����̓����p�����[�^��ǂݍ����cv::Mat�Ɋi�[

		dm->readTextureImage(textureImages, textureImageCorners);

		cout << "num of stCam: " << imgSeq_stCam_imgs.size() << endl;
		cout << "num of movCam frame: " << imgSeq_movCam.size() << endl;

		dm->readExpData(reprojected3DPoints, groundTruth2DPoints);
		//--�ē��e�덷�����߂�̂Ɏg�����_��3�������W
		//--�����̓_�̓��e�����^�l

		//for (int i = 0; i < reprojected3DPoints.size(); ++i) {
		//	cout << reprojected3DPoints.at(i) << endl;
		//}

		//for (int i = 0; i < imgSeq_stCam.size(); ++i) {
		//	me->plotPoints(imgSeq_stCam.at(i), reprojected3DPoints, extParamSeq_stCam.at(i), intParam_stCam, distCoeffs_stCam, i);
		//}

		me->run(
				imgSeq_stCam_imgs,
				extParamSeq_stCam,
				intParam_stCam,
				distCoeffs_stCam,        //--�ȏ�C�Œ�J����
				imgSeq_movCam,
				intParam_movCam,
				distCoeffs_movCam,        //--�ȏ�C�ړ��J����
				textureImages,
				textureImageCorners,
				reprojected3DPoints,
				groundTruth2DPoints
		);

	} else {
		dm->readStCamImgSeq(imgSeq_stCam, extParamSeq_stCam);            //--�Œ�J�����摜�Q��ǂݍ����vector�Ɋi�[(�O���p����)
		dm->readMovCamImgSeq(imgSeq_movCam);                            //--�ړ��J�����摜���ǂݍ����vector�Ɋi�[

		dm->readIntrinsicsStCam(intParam_stCam,
								distCoeffs_stCam);        //--�Œ�J�����̓����p�����[�^��ǂݍ����cv::Mat�Ɋi�[
		dm->readIntrinsicsMovCam(intParam_movCam,
								 distCoeffs_movCam);    //--�ړ��J�����̓����p�����[�^��ǂݍ����cv::Mat�Ɋi�[

		dm->readTextureImage(textureImages, textureImageCorners);        //--�d�􂷂�e�N�X�`���摜�Ƃ��̃R�[�i�[3�����ʒu

		if (dm->mode == "RUN") {
			me->run(
					imgSeq_stCam,
					extParamSeq_stCam,
					intParam_stCam,
					distCoeffs_stCam,        //--�ȏ�C�Œ�J����
					imgSeq_movCam,
					intParam_movCam,
					distCoeffs_movCam,        //--�ȏ�C�ړ��J����
					textureImages,
					textureImageCorners
			);
		} else if (dm->mode == "EXP") {
			cout << "num of stCam: " << imgSeq_stCam.size() << endl;
			cout << "num of movCam frame: " << imgSeq_movCam.size() << endl;

			dm->readExpData(reprojected3DPoints, groundTruth2DPoints);
			//--�ē��e�덷�����߂�̂Ɏg�����_��3�������W
			//--�����̓_�̓��e�����^�l

			//for (int i = 0; i < reprojected3DPoints.size(); ++i) {
			//	cout << reprojected3DPoints.at(i) << endl;
			//}

			//for (int i = 0; i < imgSeq_stCam.size(); ++i) {
			//	me->plotPoints(imgSeq_stCam.at(i), reprojected3DPoints, extParamSeq_stCam.at(i), intParam_stCam, distCoeffs_stCam, i);
			//}

			me->run(
					imgSeq_stCam,
					extParamSeq_stCam,
					intParam_stCam,
					distCoeffs_stCam,        //--�ȏ�C�Œ�J����
					imgSeq_movCam,
					intParam_movCam,
					distCoeffs_movCam,        //--�ȏ�C�ړ��J����
					textureImages,
					textureImageCorners,
					reprojected3DPoints,
					groundTruth2DPoints
			);
		}
	}
}