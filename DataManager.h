#include "ConfigReader.h"
#include "PhotoScanXMLReader.h"

using namespace std;

class DataManager
{
public:
	DataManager();
	~DataManager();

	void readConfig();		//--config�t�@�C���ǂݍ���
	void readStCamImgSeq(
		std::vector< cv::Mat > &argImgs,
		std::vector< cv::Mat > &argExtParams);
	void readStCamImgSeq(
		std::vector< std::vector< cv::Mat > > &argImgs,
		std::vector< cv::Mat> &argExtParams
	);
	//--�Œ�J�����摜�V�[�P���X�ǂݍ��� vector<cv::Mat> �Ɋi�[

	void readMovCamImgSeq(std::vector< cv::Mat > &argImgs);	//--�ړ��J�����摜�V�[�P���X

	void readIntrinsicsStCam(cv::Mat &argIntParams, cv::Mat &argDistCoeffs);	//--�����p�����[�^�s��ǂݍ��� cv::Mat�Ɋi�[
	void readIntrinsicsMovCam(cv::Mat &argIntParams, cv::Mat &argDistCoeffs);	//--�����p�����[�^�s��ǂݍ��� cv::Mat�Ɋi�[

	void readTextureImage(std::vector< cv::Mat > &argTexImgs, std::vector< std::vector< cv::Point3f > > &argTexImgCorners);	//--�e�N�X�`���摜�Ƃ��̃R�[�i�[��3�����ʒu�ǂݍ���
	void readExpData(std::vector< cv::Point3f > &argPts3D, std::vector< cv::Point2f > &argPts2D);				//--�ē��e����3�����ʒu�Ƃ��̓��e�^�l��ǂݍ���

	string mode;
	string pathToStCamImgSeq;			//--���摜��̃p�X
	string fileNameStCamPoses;			//--���摜�Q�̃p�X�iphotoscan��xml�t�@�C���j
	::PhotoScanCameras psCameras;		//--���낢��

	string pathToMovCamImgSeq;			//--�ړ��J�����摜��̃p�X
	string fileNameMovCamImgSeq;		//--�ړ��J�����摜��t�@�C����
	string fileNameMovCamIntParams;		//--�ړ��J���������p�����[�^

	string imageFeature;					//--�摜������

	string pathToTexture;				//--�e�N�X�`���摜�Ƃ��̊p��3�����ʒu
	string fileNameTexture1;
	string fileNameTexture2;
	string fileNameTexture3;
	string fileNameTextureCorners;

	string pathToExpData;
	string fileNameReproj3DPts;
	string fileNameGroundTruth;

	//cv::Ptr<cv::AKAZE> featureDetectorAndComputer;

private:


};