#include "Method.h"

Method::Method()
{
#ifdef __SURF
	featureDetectorAndComputer = cv::xfeatures2d::SURF::create();
#else
	featureDetectorAndComputer = cv::AKAZE::create();
#endif
	matcher = cv::DescriptorMatcher::create("BruteForce");
}

Method::~Method()
{
}

void Method::run(
	std::vector< cv::Mat > &stCamImgSeq,
	std::vector< cv::Mat > &stCamExtParams,
	cv::Mat stCamIntParams,
	cv::Mat stCamDistCoeffs,
	std::vector< cv::Mat > &movCamImgSeq,
	cv::Mat movCamIntParams,
	cv::Mat movCamDistCoeffs,
	std::vector< cv::Mat > &texImgs,
	std::vector< std::vector< cv::Point3f > > &texImgCorners
	)
{
	//--�Œ�J�����摜�Q�̐���vector���T�C�Y
	stCamImgKeypointArrays.resize(stCamImgSeq.size());
	stCamImgFeatures.resize(stCamImgSeq.size());
	stCamImgKptPos_Undis.resize(stCamImgSeq.size());
	correctMatches.resize(stCamImgSeq.size());
	correctQueryIndices.resize(stCamImgSeq.size());

	//--�Œ�J�����摜�̓����_���o�E�Ђ��݂Ȃ��̉摜���W�ʒu�v�Z
	for (int cam = 0; cam < stCamImgSeq.size(); ++cam) {
		detectAndExtractFeatures(stCamImgSeq.at(cam), stCamImgKeypointArrays.at(cam), stCamImgFeatures.at(cam));
		stCamImgKptPos_Undis.at(cam) = undistortKeypointPos(stCamImgKeypointArrays.at(cam), stCamIntParams, stCamDistCoeffs);
	}

	frameNum = movCamImgSeq.size();
	for (int frame = 0; frame < frameNum; ++frame) {

		cv::Mat tmpMovCamImg = movCamImgSeq.at(frame);
		movCamImgKeypointArray.clear();
		movCamImgFeature.~Mat();
		extParamCandidates.clear();

		//--�ړ��J�����摜�̓����_���o�E�Ђ��݂Ȃ��̉摜���W�ʒu�v�Z
		detectAndExtractFeatures(tmpMovCamImg, movCamImgKeypointArray, movCamImgFeature);
		movCamImgKptPos_Undis = undistortKeypointPos(movCamImgKeypointArray, movCamIntParams, movCamDistCoeffs);

		//--�ړ��J�����摜�ƌŒ�J����cam�̉摜�Ƃ̊Ԃł̃}�b�`���O
		double threshold_EMat = static_cast<double>(5.0f / (stCamIntParams.at<float>(0, 0) + stCamIntParams.at<float>(1, 1)) / 2.0f);
#pragma omp parallel for
		for (int cam = 0; cam < stCamImgSeq.size(); ++cam) {
			findCorresByEMat(cam, threshold_EMat);
		}

		for (int cam1 = 0; cam1 < stCamImgSeq.size() - 1; ++cam1) {
			for (int cam2 = cam1 + 1; cam2 < stCamImgSeq.size(); ++cam2) {

				std::vector< cv::Point2f > kpt2D_stCam1, kpt2D_stCam2, kpt2D_movCam;
				std::vector< cv::Point3f > kpt3D_movCam;
				cv::Mat tmpExtParam = cv::Mat::eye(4, 4, CV_32F);

				findCorresBy2D2D(cam1, cam2, kpt2D_stCam1, kpt2D_stCam2, kpt2D_movCam, stCamIntParams, movCamIntParams);

				if (6 <= kpt2D_movCam.size()) {
					calc3DPts(stCamIntParams, stCamExtParams.at(cam1), stCamExtParams.at(cam2), kpt2D_stCam1, kpt2D_stCam2, kpt3D_movCam);
					tmpExtParam = calcCamPose(kpt2D_movCam, kpt3D_movCam, movCamIntParams, movCamDistCoeffs, tmpMovCamImg);
				}
				
				extParamCandidates.push_back(tmpExtParam);
			}
		}

		int usedCam1, usedCam2;
		extParam_selected = selectProperCamPose(stCamExtParams, stCamIntParams, movCamIntParams);

		//for (int i = 0; i < texImgs.size(); ++i) {
		//    projectImage(tmpMovCamImg, texImgs.at(i), texImgCorners.at(i), extParam_selected, movCamIntParams, movCamDistCoeffs);
		//}

		cv::imshow("Result", tmpMovCamImg);
		cv::waitKey(2);
	}
}

void Method::run(
	std::vector< cv::Mat > &stCamImgSeq,
	std::vector< cv::Mat > &stCamExtParams,
	cv::Mat stCamIntParams,
	cv::Mat stCamDistCoeffs,
	std::vector< cv::Mat > &movCamImgSeq,
	cv::Mat movCamIntParams,
	cv::Mat movCamDistCoeffs,
	std::vector< cv::Mat > &texImgs,
	std::vector< std::vector< cv::Point3f > > &texImgCorners,
	std::vector< cv::Point3f > reproj3DPts,
	std::vector< cv::Point2f > gT2DPts
	)
{

	//--�Œ�J�����摜�Q�̐���vector���T�C�Y
	stCamImgKeypointArrays.resize(stCamImgSeq.size());
	stCamImgFeatures.resize(stCamImgSeq.size());
	stCamImgKptPos_Undis.resize(stCamImgSeq.size());
	correctMatches.resize(stCamImgSeq.size());
	correctQueryIndices.resize(stCamImgSeq.size());

	//--�Œ�J�����摜�̓����_���o�E�Ђ��݂Ȃ��̉摜���W�ʒu�v�Z
	for (int cam = 0; cam < stCamImgSeq.size(); ++cam) {
		detectAndExtractFeatures(stCamImgSeq.at(cam), stCamImgKeypointArrays.at(cam), stCamImgFeatures.at(cam));
		cout << "stCam:" << cam << " keypointNum:" << stCamImgKeypointArrays.at(cam).size() << endl;
		stCamImgKptPos_Undis.at(cam) = undistortKeypointPos(stCamImgKeypointArrays.at(cam), stCamIntParams, stCamDistCoeffs);
	}

	frameNum = movCamImgSeq.size();
  	for (int frame = 0; frame < frameNum; ++frame) {

		cv::Mat tmpMovCamImg = movCamImgSeq.at(frame);
		movCamImgKeypointArray.clear();
		movCamImgFeature.~Mat();
		extParamCandidates.clear();

		detectAndExtractFeatures(tmpMovCamImg, movCamImgKeypointArray, movCamImgFeature);
		cout << "movCam frame:" << frame << " keypointNum:" << movCamImgKeypointArray.size() << endl;
		movCamImgKptPos_Undis = undistortKeypointPos(movCamImgKeypointArray, movCamIntParams, movCamDistCoeffs);

		double threshold_EMat = static_cast<double>(5.0f / (stCamIntParams.at<float>(0, 0) + stCamIntParams.at<float>(1, 1)) / 2.0f);
#pragma omp parallel for
		for (int cam = 0; cam < stCamImgSeq.size(); ++cam) {
			findCorresByEMat(cam, threshold_EMat, stCamImgSeq.at(cam), tmpMovCamImg);
		}
		for (int cam = 0; cam < stCamImgSeq.size(); ++cam) {
			cout << "stCam" << cam << " correctMatches.size(): " << correctMatches.at(cam).size() << endl;
		}

		for (int cam1 = 0; cam1 < stCamImgSeq.size() - 1; ++cam1) {
			for (int cam2 = cam1 + 1; cam2 < stCamImgSeq.size(); ++cam2) {

				std::vector< cv::Point2f > kpt2D_stCam1, kpt2D_stCam2, kpt2D_movCam;
				std::vector< cv::Point3f > kpt3D_movCam;
				cv::Mat tmpExtParam = cv::Mat::eye(4, 4, CV_32F);

				findCorresBy2D2D(cam1, cam2, kpt2D_stCam1, kpt2D_stCam2, kpt2D_movCam, stCamIntParams, movCamIntParams, stCamImgSeq.at(cam1), stCamImgSeq.at(cam2), tmpMovCamImg);

				if (6 <= kpt2D_movCam.size()) {
					calc3DPts(stCamIntParams, stCamExtParams.at(cam1), stCamExtParams.at(cam2), kpt2D_stCam1, kpt2D_stCam2, kpt3D_movCam);
					tmpExtParam = calcCamPose(kpt2D_movCam, kpt3D_movCam, movCamIntParams, movCamDistCoeffs, tmpMovCamImg);
					//plotPoints(tmpMovCamImg, kpt3D_movCam, tmpExtParam, movCamIntParams, movCamDistCoeffs, frame);
				}

				extParamCandidates.push_back(tmpExtParam);
			}
		}

		int usedCam1, usedCam2;
		extParam_selected = selectProperCamPose(&usedCam1, &usedCam2, stCamExtParams, stCamIntParams, movCamIntParams);

		cout << "selected: " << usedCam1 << ", " << usedCam2 << endl;

		//for (int i = 0; i < texImgs.size(); ++i) {
		//	projectImage(tmpMovCamImg, texImgs.at(i), texImgCorners.at(i), extParam_selected, movCamIntParams, movCamDistCoeffs);
		//}
		//stringstream ss("");
		//ss << "./AR/AR_" << frame << ".png";
		//cv::imwrite(ss.str(), tmpMovCamImg);

		plotPoints(tmpMovCamImg, reproj3DPts, extParam_selected, movCamIntParams, movCamDistCoeffs, frame);
	}

	cv::waitKey();
}

void Method::run(std::vector<std::vector<cv::Mat> > &stCamImgSeq, std::vector<cv::Mat> &stCamExtParams,
                 cv::Mat stCamIntParams, cv::Mat stCamDistCoeffs, std::vector<cv::Mat> &movCamImgSeq,
                 cv::Mat movCamIntParams, cv::Mat movCamDistCoeffs, std::vector<cv::Mat> &texImgs,
                 std::vector<std::vector<cv::Point3f> > &texImgCorners, std::vector<cv::Point3f> reproj3DPts,
                 std::vector<cv::Point2f> gT2DPts) {

    stCamImgKeypointArrays.resize(stCamImgSeq.size());
    stCamImgFeatures.resize(stCamImgSeq.size());
    stCamImgKptPos_Undis.resize(stCamImgSeq.size());
    correctMatches.resize(stCamImgSeq.size());
    correctQueryIndices.resize(stCamImgSeq.size());

    frameNum = movCamImgSeq.size();
    for (int frame = 0; frame < frameNum; ++frame) {

        for (int cam = 0; cam < stCamImgSeq.size(); ++cam) {
            detectAndExtractFeatures(stCamImgSeq.at(cam).at(frame), stCamImgKeypointArrays.at(cam), stCamImgFeatures.at(cam));
            cout << "stCam:" << cam << " keypointNum:" << stCamImgKeypointArrays.at(cam).size() << endl;
            stCamImgKptPos_Undis.at(cam) = undistortKeypointPos(stCamImgKeypointArrays.at(cam), stCamIntParams, stCamDistCoeffs);
        }


        cv::Mat tmpMovCamImg = movCamImgSeq.at(frame);
        movCamImgKeypointArray.clear();
        movCamImgFeature.~Mat();
        extParamCandidates.clear();

        detectAndExtractFeatures(tmpMovCamImg, movCamImgKeypointArray, movCamImgFeature);
        cout << "movCam frame:" << frame << " keypointNum:" << movCamImgKeypointArray.size() << endl;
        movCamImgKptPos_Undis = undistortKeypointPos(movCamImgKeypointArray, movCamIntParams, movCamDistCoeffs);

        double threshold_EMat = static_cast<double>(5.0f / (stCamIntParams.at<float>(0, 0) + stCamIntParams.at<float>(1, 1)) / 2.0f);
#pragma omp parallel for
        for (int cam = 0; cam < stCamImgSeq.size(); ++cam) {
            findCorresByEMat(cam, threshold_EMat, stCamImgSeq.at(cam), tmpMovCamImg);
        }
        for (int cam = 0; cam < stCamImgSeq.size(); ++cam) {
            cout << "stCam" << cam << " correctMatches.size(): " << correctMatches.at(cam).size() << endl;
        }

        for (int cam1 = 0; cam1 < stCamImgSeq.size() - 1; ++cam1) {
            for (int cam2 = cam1 + 1; cam2 < stCamImgSeq.size(); ++cam2) {

                std::vector< cv::Point2f > kpt2D_stCam1, kpt2D_stCam2, kpt2D_movCam;
                std::vector< cv::Point3f > kpt3D_movCam;
                cv::Mat tmpExtParam = cv::Mat::eye(4, 4, CV_32F);

                findCorresBy2D2D(cam1, cam2, kpt2D_stCam1, kpt2D_stCam2, kpt2D_movCam, stCamIntParams, movCamIntParams, stCamImgSeq.at(cam1), stCamImgSeq.at(cam2), tmpMovCamImg);

                if (6 <= kpt2D_movCam.size()) {
                    calc3DPts(stCamIntParams, stCamExtParams.at(cam1), stCamExtParams.at(cam2), kpt2D_stCam1, kpt2D_stCam2, kpt3D_movCam);
                    tmpExtParam = calcCamPose(kpt2D_movCam, kpt3D_movCam, movCamIntParams, movCamDistCoeffs, tmpMovCamImg);
                    //plotPoints(tmpMovCamImg, kpt3D_movCam, tmpExtParam, movCamIntParams, movCamDistCoeffs, frame);
                }

                extParamCandidates.push_back(tmpExtParam);
            }
        }

        int usedCam1, usedCam2;
        extParam_selected = selectProperCamPose(&usedCam1, &usedCam2, stCamExtParams, stCamIntParams, movCamIntParams);

        cout << "selected: " << usedCam1 << ", " << usedCam2 << endl;

        //for (int i = 0; i < texImgs.size(); ++i) {
        //	projectImage(tmpMovCamImg, texImgs.at(i), texImgCorners.at(i), extParam_selected, movCamIntParams, movCamDistCoeffs);
        //}
        //stringstream ss("");
        //ss << "./AR/AR_" << frame << ".png";
        //cv::imwrite(ss.str(), tmpMovCamImg);

        plotPoints(tmpMovCamImg, reproj3DPts, extParam_selected, movCamIntParams, movCamDistCoeffs, frame);

        stCamImgKeypointArrays.clear();
        stCamImgFeatures.clear();
        stCamImgKptPos_Undis.clear();
        correctMatches.clear();
        correctQueryIndices.clear();
    }

    cv::waitKey();
}

void Method::detectFeatures()
{

}

void Method::extractFeatures()
{

}

void Method::detectAndExtractFeatures(cv::Mat tmpImg, vector< cv::KeyPoint > &keypointArray, cv::Mat &features)
{
	featureDetectorAndComputer->detectAndCompute(tmpImg, cv::noArray(), keypointArray, features);
}

cv::Mat Method::undistortKeypointPos(const vector< cv::KeyPoint > &keypoints, cv::Mat iParam, cv::Mat distCoeffs)
{
	cv::Mat kpts_undistorted = cv::Mat::ones(3, keypoints.size(), CV_32F);
	cv::Mat src(keypoints.size(), 1, CV_32FC2), dst;

	for (int j = 0; j < keypoints.size(); ++j) {
		src.at<cv::Point2f>(j, 0) = keypoints.at(j).pt;
	}
	cv::undistortPoints(src, dst, iParam, distCoeffs);			//undistortPoints�͓_��x1, 2ch

	dst = (dst).reshape(1).t();									//undistortPoints��2x�_��, 1ch
	dst.copyTo(kpts_undistorted.rowRange(cv::Range(0, 2)));

	return kpts_undistorted;
}

void Method::findCorresByEMat(int cam, double threshold)
{
	//--�N���X�}�b�`���O���g����2D2D�Ή��擾
	std::vector< cv::DMatch > matches_Nearest, matches_CrossMatch;
	std::vector< int > index_CrossMatch;
	cv::Mat trainIdxFromSt = cv::Mat::ones(stCamImgFeatures.at(cam).rows, 1, CV_32S) * -1;	//--stCamImg�̓����_���猩��movCamImg�̍ŋߖT�����_
	cv::Mat isUsed_st = cv::Mat::zeros(stCamImgFeatures.at(cam).rows, 1, CV_8U);			//--stCamImg�̓����_�����łɃ}�b�`���O����������
	matcher->match(movCamImgFeature, stCamImgFeatures.at(cam), matches_Nearest);			//--movCamImg�̓����_����stCamImg�̓����_�ւ̍ŋߖT�T��
	for (int i = 0; i < matches_Nearest.size(); ++i) {
		int tmpTrainIdx = matches_Nearest.at(i).trainIdx;
		bool isMatched = false;

		if (isUsed_st.at<uchar>(tmpTrainIdx, 0) != 0)
			continue;

		if (trainIdxFromSt.at<int>(tmpTrainIdx, 0) == matches_Nearest.at(i).queryIdx) {		//--�N���X�}�b�`����
			isMatched = true;
		}
		else {
			cv::Mat tmpFeature_stCam = stCamImgFeatures.at(cam).row(tmpTrainIdx);
			std::vector< cv::DMatch > tmpMatch;
			matcher->match(tmpFeature_stCam, movCamImgFeature, tmpMatch);
			trainIdxFromSt.at<int>(tmpTrainIdx, 0) = tmpMatch.at(0).trainIdx;

			if (tmpMatch.at(0).trainIdx == matches_Nearest.at(i).queryIdx) {				//--�N���X�}�b�`����
				isMatched = true;
			}
		}

		if (isMatched) {
			matches_CrossMatch.push_back(matches_Nearest.at(i));
			index_CrossMatch.push_back(i);
			isUsed_st.at<uchar>(tmpTrainIdx, 0) = 1;
		}
	}

	//--Ransac���g����2D2D�Ή��擾�i���Ή��擾�j
	cv::Mat stCamImgKpt2D_Mat(matches_CrossMatch.size(), 2, CV_32F);
	cv::Mat movCamImgKpt2D_Mat(matches_CrossMatch.size(), 2, CV_32F);
	//cout << stCamImgKptPos_Undis.at(cam) << endl;
	for (int i = 0; i < matches_CrossMatch.size(); ++i) {
		int tmpTrainIdx = matches_CrossMatch.at(i).trainIdx;
		int tmpQueryIdx = matches_CrossMatch.at(i).queryIdx;
		stCamImgKpt2D_Mat.at<float>(i, 0) = stCamImgKptPos_Undis.at(cam).at<float>(0, tmpTrainIdx);
		stCamImgKpt2D_Mat.at<float>(i, 1) = stCamImgKptPos_Undis.at(cam).at<float>(1, tmpTrainIdx);
		movCamImgKpt2D_Mat.at<float>(i, 0) = movCamImgKptPos_Undis.at<float>(0, tmpQueryIdx);
		movCamImgKpt2D_Mat.at<float>(i, 1) = movCamImgKptPos_Undis.at<float>(1, tmpQueryIdx);
	}

	//--���̂Ƃ��C�����p�����[�^�͒P�ʍs��
	cv::Mat iParam_identity = cv::Mat::eye(3, 3, CV_32F);
	cv::Mat mask;
	cv::Mat essentialMat = cv::findEssentialMat(stCamImgKpt2D_Mat, movCamImgKpt2D_Mat, iParam_identity, cv::RANSAC, 0.999f, threshold, mask);

	//--���Ή����o��
	correctMatches.at(cam).clear();
	correctQueryIndices.at(cam).clear();
	for (int i = 0; i < mask.rows; ++i) {
		if (mask.at<uchar>(i, 0)) {
			correctMatches.at(cam).push_back(matches_CrossMatch.at(i));
			correctQueryIndices.at(cam).push_back(index_CrossMatch.at(i));
		}
	}
}

void Method::findCorresByEMat(int cam, double threshold, cv::Mat stCamImg, cv::Mat movCamImg)
{
	//--�N���X�}�b�`���O���g����2D2D�Ή��擾
	std::vector< cv::DMatch > matches_Nearest, matches_CrossMatch;
	std::vector< int > index_CrossMatch;
	cv::Mat trainIdxFromSt = cv::Mat::ones(stCamImgFeatures.at(cam).rows, 1, CV_32S) * -1;	//--stCamImg�̓����_���猩��movCamImg�̍ŋߖT�����_
	cv::Mat isUsed_st = cv::Mat::zeros(stCamImgFeatures.at(cam).rows, 1, CV_8U);			//--stCamImg�̓����_�����łɃ}�b�`���O����������

	matcher->match(movCamImgFeature, stCamImgFeatures.at(cam), matches_Nearest);			//--movCamImg�̓����_����stCamImg�̓����_�ւ̍ŋߖT�T��
	for (int i = 0; i < matches_Nearest.size(); ++i) {
		int tmpTrainIdx = matches_Nearest.at(i).trainIdx;
		bool isMatched = false;

		if (isUsed_st.at<uchar>(tmpTrainIdx, 0) != 0)
			continue;

		if (trainIdxFromSt.at<int>(tmpTrainIdx, 0) == matches_Nearest.at(i).queryIdx) {		//--�N���X�}�b�`����
			isMatched = true;
		}
		else {
			cv::Mat tmpFeature_stCam = stCamImgFeatures.at(cam).row(tmpTrainIdx);
			std::vector< cv::DMatch > tmpMatch;
			matcher->match(tmpFeature_stCam, movCamImgFeature, tmpMatch);
			trainIdxFromSt.at<int>(tmpTrainIdx, 0) = tmpMatch.at(0).trainIdx;

			if (tmpMatch.at(0).trainIdx == matches_Nearest.at(i).queryIdx) {				//--�N���X�}�b�`����
				isMatched = true;
			}
		}

		if (isMatched) {
			matches_CrossMatch.push_back(matches_Nearest.at(i));
			index_CrossMatch.push_back(i);
			isUsed_st.at<uchar>(tmpTrainIdx, 0) = 1;
		}
	}

	//--Ransac���g����2D2D�Ή��擾�i���Ή��擾�j
	cv::Mat stCamImgKpt2D_Mat(matches_CrossMatch.size(), 2, CV_32F);
	cv::Mat movCamImgKpt2D_Mat(matches_CrossMatch.size(), 2, CV_32F);
	//cout << stCamImgKptPos_Undis.at(cam) << endl;
	for (int i = 0; i < matches_CrossMatch.size(); ++i) {
		int tmpTrainIdx = matches_CrossMatch.at(i).trainIdx;
		int tmpQueryIdx = matches_CrossMatch.at(i).queryIdx;
		stCamImgKpt2D_Mat.at<float>(i, 0) = stCamImgKptPos_Undis.at(cam).at<float>(0, tmpTrainIdx);
		stCamImgKpt2D_Mat.at<float>(i, 1) = stCamImgKptPos_Undis.at(cam).at<float>(1, tmpTrainIdx);
		movCamImgKpt2D_Mat.at<float>(i, 0) = movCamImgKptPos_Undis.at<float>(0, tmpQueryIdx);
		movCamImgKpt2D_Mat.at<float>(i, 1) = movCamImgKptPos_Undis.at<float>(1, tmpQueryIdx);
	}

	//--���̂Ƃ��C�����p�����[�^�͒P�ʍs��
	cv::Mat iParam_identity = cv::Mat::eye(3, 3, CV_32F);
	cv::Mat mask;
	cv::Mat essentialMat = cv::findEssentialMat(stCamImgKpt2D_Mat, movCamImgKpt2D_Mat, iParam_identity, cv::RANSAC, 0.999f, threshold, mask);

	//--���Ή����o��
	correctMatches.at(cam).clear();
	correctQueryIndices.at(cam).clear();

	for (int i = 0; i < mask.rows; ++i) {
		if (mask.at<uchar>(i, 0)) {
			correctMatches.at(cam).push_back(matches_CrossMatch.at(i));
			correctQueryIndices.at(cam).push_back(index_CrossMatch.at(i));
		}
	}

	cv::Mat crossMatchImg;
	cv::drawMatches(movCamImg, movCamImgKeypointArray, stCamImg, stCamImgKeypointArrays.at(cam), matches_CrossMatch, crossMatchImg);
	cv::Mat RansacMatchImg;
	cv::drawMatches(movCamImg, movCamImgKeypointArray, stCamImg, stCamImgKeypointArrays.at(cam), correctMatches.at(cam), RansacMatchImg, cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 255));
	cv::Mat compareMatchImg;
	cv::vconcat(crossMatchImg, RansacMatchImg, compareMatchImg);
	cv::imshow("compare Match" + to_string(cam), compareMatchImg);
	cv::waitKey(2);
}

void Method::findCorresBy2D2D(
	int cam1, int cam2, 
	vector< cv::Point2f > &pt2DArray1, vector< cv::Point2f > &pt2DArray2, 
	vector< cv::Point2f > &pt2DArray3, 
	cv::Mat iParam_stCam, cv::Mat iParam_movCam)
{
	int j_start = 0;
	for (int i = 0; i < correctQueryIndices.at(cam1).size(); ++i) {
		for (int j = j_start; j < correctQueryIndices.at(cam2).size(); ++j) {

			int queryIdx_cam1 = correctQueryIndices.at(cam1).at(i);
			int queryIdx_cam2 = correctQueryIndices.at(cam2).at(j);

			if (queryIdx_cam2 < queryIdx_cam1)
				continue;
			else if (queryIdx_cam1 < queryIdx_cam2)
				break;


			int trainIdx_cam1 = correctMatches.at(cam1).at(i).trainIdx;
			int trainIdx_cam2 = correctMatches.at(cam2).at(j).trainIdx;

			//--�����őS���Ђ��݂Ȃ��摜���W�ɕϊ�
			cv::Mat pt2D1_cvMat = iParam_stCam * stCamImgKptPos_Undis.at(cam1)(cv::Rect(trainIdx_cam1, 0, 1, 3));
			cv::Mat pt2D2_cvMat = iParam_stCam * stCamImgKptPos_Undis.at(cam2)(cv::Rect(trainIdx_cam2, 0, 1, 3));
			cv::Mat pt2D3_cvMat = iParam_movCam * movCamImgKptPos_Undis(cv::Rect(queryIdx_cam1, 0, 1, 3));
			cv::Point2f pt2D1(pt2D1_cvMat.at<float>(0, 0), pt2D1_cvMat.at<float>(1, 0));
			cv::Point2f pt2D2(pt2D2_cvMat.at<float>(0, 0), pt2D2_cvMat.at<float>(1, 0));
			cv::Point2f pt2D3(pt2D3_cvMat.at<float>(0, 0), pt2D3_cvMat.at<float>(1, 0));

			pt2DArray1.push_back(pt2D1);
			pt2DArray2.push_back(pt2D2);
			pt2DArray3.push_back(pt2D3);

			j_start = j + 1;
			break;
		}
	}
}

void Method::findCorresBy2D2D(
	int cam1, int cam2,
	vector< cv::Point2f > &pt2DArray1, vector< cv::Point2f > &pt2DArray2,
	vector< cv::Point2f > &pt2DArray3,
	cv::Mat iParam_stCam, cv::Mat iParam_movCam,
	cv::Mat argCamImg1, cv::Mat argCamImg2, cv::Mat argCamImg3)
{
	vector< cv::Point2f > pts_stCam1(0);
	vector< cv::Point2f > pts_stCam2(0);
	vector< cv::Point2f > pts_movCam(0);

	int j_start = 0;
	for (int i = 0; i < correctQueryIndices.at(cam1).size(); ++i) {
		for (int j = j_start; j < correctQueryIndices.at(cam2).size(); ++j) {

			int queryIdx_cam1 = correctQueryIndices.at(cam1).at(i);
			int queryIdx_cam2 = correctQueryIndices.at(cam2).at(j);

			if (queryIdx_cam2 < queryIdx_cam1)
				continue;
			else if (queryIdx_cam1 < queryIdx_cam2)
				break;

			int trainIdx_cam1 = correctMatches.at(cam1).at(i).trainIdx;
			int trainIdx_cam2 = correctMatches.at(cam2).at(j).trainIdx;

			//--�����őS���Ђ��݂Ȃ��摜���W�ɕϊ�
			cv::Mat pt2D1_cvMat = iParam_stCam * stCamImgKptPos_Undis.at(cam1)(cv::Rect(trainIdx_cam1, 0, 1, 3));
			cv::Mat pt2D2_cvMat = iParam_stCam * stCamImgKptPos_Undis.at(cam2)(cv::Rect(trainIdx_cam2, 0, 1, 3));
			cv::Mat pt2D3_cvMat = iParam_movCam * movCamImgKptPos_Undis(cv::Rect(queryIdx_cam1, 0, 1, 3));
			cv::Point2f pt2D1(pt2D1_cvMat.at<float>(0, 0), pt2D1_cvMat.at<float>(1, 0));
			cv::Point2f pt2D2(pt2D2_cvMat.at<float>(0, 0), pt2D2_cvMat.at<float>(1, 0));
			cv::Point2f pt2D3(pt2D3_cvMat.at<float>(0, 0), pt2D3_cvMat.at<float>(1, 0));

			pt2DArray1.push_back(pt2D1);
			pt2DArray2.push_back(pt2D2);
			pt2DArray3.push_back(pt2D3);

			pts_stCam1.push_back(stCamImgKeypointArrays.at(cam1).at(trainIdx_cam1).pt);
			pts_stCam2.push_back(stCamImgKeypointArrays.at(cam2).at(trainIdx_cam2).pt);
			pts_movCam.push_back(movCamImgKeypointArray.at(queryIdx_cam1).pt);

			j_start = j + 1;
			break;
		}
	}

	cv::Mat stCamImg1 = argCamImg1.clone();
	cv::Mat movCamImg = argCamImg3.clone();
	cv::Mat stCamImg2 = argCamImg2.clone();

	cv::Mat outputImg;
	cv::hconcat(stCamImg1, movCamImg, outputImg);
	cv::hconcat(outputImg, stCamImg2, outputImg);
	for (int i = 0; i < pts_stCam1.size(); ++i) {
		int blue = rand() % 255;
		int green = rand() % 255;
		int red = rand() % 255;
		cv::Scalar color = cv::Scalar(blue, green, red);
		cv::circle(outputImg, pts_stCam1.at(i), 2, color, 2);
		cv::circle(outputImg, cv::Point2f(pts_movCam.at(i).x + stCamImg1.cols, pts_movCam.at(i).y), 2, color, 2);
		cv::putText(outputImg, to_string(i), cv::Point2f(pts_movCam.at(i).x + stCamImg1.cols, pts_movCam.at(i).y), CV_FONT_HERSHEY_PLAIN, 1, color);
		cv::circle(outputImg, cv::Point2f(pts_stCam2.at(i).x + stCamImg1.cols + movCamImg.cols, pts_stCam2.at(i).y), 2, color, 2);
		cv::line(outputImg, pts_stCam1.at(i), cv::Point2f(pts_movCam.at(i).x + stCamImg1.cols, pts_movCam.at(i).y), color, 1, CV_AA);
		cv::line(outputImg, cv::Point2f(pts_movCam.at(i).x + stCamImg1.cols, pts_movCam.at(i).y), cv::Point2f(pts_stCam2.at(i).x + stCamImg1.cols + movCamImg.cols, pts_stCam2.at(i).y), color, 1, CV_AA);
	}

	cv::imshow("2D2D matching", outputImg);
	cout << cam1 << ", " << cam2 << " corresponding match : " << pts_stCam1.size() << endl;
	cv::waitKey(2);
}


void Method::calc3DPts(
	cv::Mat stCamIntParam,
	cv::Mat stCamExtParam1, cv::Mat stCamExtParam2,
	const vector< cv::Point2f > &pt2DArray1, const vector< cv::Point2f > &pt2DArray2,
	vector < cv::Point3f > &pt3DArray)
{
	for (int i = 0; i < pt2DArray1.size(); ++i) {

		//--2�̌Œ�J������PMat(4x4)�����߂�
		cv::Mat intParam_stCam_4x4 = cv::Mat::eye(4, 4, CV_32F);
		stCamIntParam.copyTo(intParam_stCam_4x4(cv::Rect(0, 0, 3, 3)));
		cv::Mat pMat_cam1 = intParam_stCam_4x4 * stCamExtParam1;
		cv::Mat pMat_cam2 = intParam_stCam_4x4 * stCamExtParam2;

		cv::Mat B(4, 3, CV_32F);
		cv::Mat q(4, 1, CV_32F);
		cv::Mat pt3D_Mat(3, 1, CV_32F);

		B.at<float>(0, 0) = pMat_cam1.at<float>(2, 0) * pt2DArray1.at(i).x - pMat_cam1.at<float>(0, 0);
		B.at<float>(0, 1) = pMat_cam1.at<float>(2, 1) * pt2DArray1.at(i).x - pMat_cam1.at<float>(0, 1);
		B.at<float>(0, 2) = pMat_cam1.at<float>(2, 2) * pt2DArray1.at(i).x - pMat_cam1.at<float>(0, 2);
		B.at<float>(1, 0) = pMat_cam1.at<float>(2, 0) * pt2DArray1.at(i).y - pMat_cam1.at<float>(1, 0);
		B.at<float>(1, 1) = pMat_cam1.at<float>(2, 1) * pt2DArray1.at(i).y - pMat_cam1.at<float>(1, 1);
		B.at<float>(1, 2) = pMat_cam1.at<float>(2, 2) * pt2DArray1.at(i).y - pMat_cam1.at<float>(1, 2);
		q.at<float>(0, 0) = pMat_cam1.at<float>(0, 3) - pMat_cam1.at<float>(2, 3) * pt2DArray1.at(i).x;
		q.at<float>(1, 0) = pMat_cam1.at<float>(1, 3) - pMat_cam1.at<float>(2, 3) * pt2DArray1.at(i).y;

		B.at<float>(2, 0) = pMat_cam2.at<float>(2, 0) * pt2DArray2.at(i).x - pMat_cam2.at<float>(0, 0);
		B.at<float>(2, 1) = pMat_cam2.at<float>(2, 1) * pt2DArray2.at(i).x - pMat_cam2.at<float>(0, 1);
		B.at<float>(2, 2) = pMat_cam2.at<float>(2, 2) * pt2DArray2.at(i).x - pMat_cam2.at<float>(0, 2);
		B.at<float>(3, 0) = pMat_cam2.at<float>(2, 0) * pt2DArray2.at(i).y - pMat_cam2.at<float>(1, 0);
		B.at<float>(3, 1) = pMat_cam2.at<float>(2, 1) * pt2DArray2.at(i).y - pMat_cam2.at<float>(1, 1);
		B.at<float>(3, 2) = pMat_cam2.at<float>(2, 2) * pt2DArray2.at(i).y - pMat_cam2.at<float>(1, 2);
		q.at<float>(2, 0) = pMat_cam2.at<float>(0, 3) - pMat_cam2.at<float>(2, 3) * pt2DArray2.at(i).x;
		q.at<float>(3, 0) = pMat_cam2.at<float>(1, 3) - pMat_cam2.at<float>(2, 3) * pt2DArray2.at(i).y;

		cv::solve(B, q, pt3D_Mat, cv::DECOMP_SVD);
		pt3DArray.push_back(cv::Point3f(pt3D_Mat.at<float>(0, 0), pt3D_Mat.at<float>(1, 0), pt3D_Mat.at<float>(2, 0)));

	}

}

cv::Mat Method::calcCamPose(const vector< cv::Point2f > &pt2D, const vector< cv::Point3f > &pt3D, cv::Mat iParam, cv::Mat distCoeffs, cv::Mat tmpImage)
{
	cv::Mat Rt = cv::Mat::eye(4, 4, CV_32F);
	cv::Mat rVec, tVec, rMat;
	cv::Mat distCoeffs_null = cv::Mat::zeros(1, 5, CV_32F);
	cv::solvePnP(pt3D, pt2D, iParam, distCoeffs_null, rVec, tVec);

	cv::Rodrigues(rVec, rMat);
	rMat.convertTo(rMat, CV_32F);
	tVec.convertTo(tVec, CV_32F);
	rMat.copyTo(Rt(cv::Rect(0, 0, 3, 3)));
	tVec.copyTo(Rt(cv::Rect(3, 0, 1, 3)));

	return Rt;
}

cv::Mat Method::selectProperCamPose(
	const std::vector< cv::Mat > &extParamsArray_stCam,
	cv::Mat intParams_stCam,
	cv::Mat intParams_movCam)
{
	float threshold = 5.0f;
	int minOutlierCount = INT_MAX;
	int minOutlierExtParamIndex = 0;

	int extParamCount = 0;
	for (int cam1 = 0; cam1 < stCamImgKeypointArrays.size() - 1; ++cam1) {
		for (int cam2 = cam1 + 1; cam2 < stCamImgKeypointArrays.size(); ++cam2) {

			int outlierCount = 0;

			for (int cam = 0; cam < stCamImgKeypointArrays.size(); ++cam) {

				if (correctMatches.at(cam).size() == 0)
					continue;

				cv::Mat extParamFromMovToSt = extParamsArray_stCam.at(cam) * extParamCandidates.at(extParamCount).inv();
				cv::Mat alterMat_t = (cv::Mat_<float>(3, 3) <<				//t�̌��s��
					0.0f, -1.0f * extParamFromMovToSt.at<float>(2, 3), extParamFromMovToSt.at<float>(1, 3),
					extParamFromMovToSt.at<float>(2, 3), 0.0f, -1.0f * extParamFromMovToSt.at<float>(0, 3),
					-1.0f * extParamFromMovToSt.at<float>(1, 3), extParamFromMovToSt.at<float>(0, 3), 0.0f);
				cv::Mat essentialMat = alterMat_t * extParamFromMovToSt(cv::Rect(0, 0, 3, 3));
				cv::Mat fundamentalMat = (intParams_stCam.inv()).t() * essentialMat * (intParams_movCam.inv());

				cv::Mat usedKeypoints_movCam(3, correctMatches.at(cam).size(), CV_32F);
				cv::Mat usedKeypoints_stCam(3, correctMatches.at(cam).size(), CV_32F);
				for (int i = 0; i < correctMatches.at(cam).size(); ++i) {
					usedKeypoints_movCam.at<float>(0, i) = movCamImgKptPos_Undis.at<float>(0, correctMatches.at(cam).at(i).queryIdx);
					usedKeypoints_movCam.at<float>(1, i) = movCamImgKptPos_Undis.at<float>(1, correctMatches.at(cam).at(i).queryIdx);
					usedKeypoints_movCam.at<float>(2, i) = 1.0f;

					usedKeypoints_stCam.at<float>(0, i) = stCamImgKptPos_Undis.at(cam).at<float>(0, correctMatches.at(cam).at(i).trainIdx);
					usedKeypoints_stCam.at<float>(1, i) = stCamImgKptPos_Undis.at(cam).at<float>(1, correctMatches.at(cam).at(i).trainIdx);
					usedKeypoints_stCam.at<float>(2, i) = 1.0f;
				}

				//--���K���摜���W -> �摜���W�ɒ���
				usedKeypoints_movCam = intParams_movCam * usedKeypoints_movCam;
				usedKeypoints_stCam = intParams_stCam * usedKeypoints_stCam;

				std::vector< cv::Vec3f > lines;
				cv::computeCorrespondEpilines(usedKeypoints_movCam.t()(cv::Rect(0, 0, 2, usedKeypoints_movCam.cols)), 1, fundamentalMat, lines);

				for (int j = 0; j < lines.size(); ++j) {
					float stCamKpPosX = usedKeypoints_stCam.at<float>(0, j);
					float stCamKpPosY = usedKeypoints_stCam.at<float>(1, j);
					float error = abs(lines.at(j)[0] * stCamKpPosX + lines.at(j)[1] * stCamKpPosY + lines.at(j)[2]) / (sqrtf(powf(lines.at(j)[0], 2.0f) + powf(lines.at(j)[1], 2.0f)));

					if (threshold < error)
						++outlierCount;
				}
			}

			if (outlierCount <= minOutlierCount) {
				minOutlierCount = outlierCount;
				minOutlierExtParamIndex = extParamCount;
			}

			++extParamCount;
		}
	}

	return extParamCandidates.at(minOutlierExtParamIndex);
}

cv::Mat Method::selectProperCamPose(
	int *selectedCam1, int *selectedCam2,
	const std::vector< cv::Mat > &extParamsArray_stCam,
	cv::Mat intParams_stCam,
	cv::Mat intParams_movCam)
{ 
	float threshold = 5.0f;
	int minOutlierCount = INT_MAX;
	int minOutlierExtParamIndex = 0;

	int extParamCount = 0;
	for (int cam1 = 0; cam1 < stCamImgKeypointArrays.size() - 1; ++cam1) {
		for (int cam2 = cam1 + 1; cam2 < stCamImgKeypointArrays.size(); ++cam2) {

			int outlierCount = 0;

			for (int cam = 0; cam < stCamImgKeypointArrays.size(); ++cam) {

				if (correctMatches.at(cam).size() == 0)
					continue;
				
				cv::Mat extParamFromMovToSt = extParamsArray_stCam.at(cam) * extParamCandidates.at(extParamCount).inv();
				cv::Mat alterMat_t = (cv::Mat_<float>(3, 3) <<				//t�̌��s��
					0.0f, -1.0f * extParamFromMovToSt.at<float>(2, 3), extParamFromMovToSt.at<float>(1, 3),
					extParamFromMovToSt.at<float>(2, 3), 0.0f, -1.0f * extParamFromMovToSt.at<float>(0, 3),
					-1.0f * extParamFromMovToSt.at<float>(1, 3), extParamFromMovToSt.at<float>(0, 3), 0.0f);
				cv::Mat essentialMat = alterMat_t * extParamFromMovToSt(cv::Rect(0, 0, 3, 3));					
				cv::Mat fundamentalMat = (intParams_stCam.inv()).t() * essentialMat * (intParams_movCam.inv());
			
				cv::Mat usedKeypoints_movCam(3, correctMatches.at(cam).size(), CV_32F);
				cv::Mat usedKeypoints_stCam(3, correctMatches.at(cam).size(), CV_32F);
				for (int i = 0; i < correctMatches.at(cam).size(); ++i) {
					usedKeypoints_movCam.at<float>(0, i) = movCamImgKptPos_Undis.at<float>(0, correctMatches.at(cam).at(i).queryIdx);
					usedKeypoints_movCam.at<float>(1, i) = movCamImgKptPos_Undis.at<float>(1, correctMatches.at(cam).at(i).queryIdx);
					usedKeypoints_movCam.at<float>(2, i) = 1.0f;

					usedKeypoints_stCam.at<float>(0, i) = stCamImgKptPos_Undis.at(cam).at<float>(0, correctMatches.at(cam).at(i).trainIdx);
					usedKeypoints_stCam.at<float>(1, i) = stCamImgKptPos_Undis.at(cam).at<float>(1, correctMatches.at(cam).at(i).trainIdx);
					usedKeypoints_stCam.at<float>(2, i) = 1.0f;
				}

				//--���K���摜���W -> �摜���W�ɒ���
				usedKeypoints_movCam = intParams_movCam * usedKeypoints_movCam;
				usedKeypoints_stCam = intParams_stCam * usedKeypoints_stCam;

				std::vector< cv::Vec3f > lines;
				cv::computeCorrespondEpilines(usedKeypoints_movCam.t()(cv::Rect(0, 0, 2, usedKeypoints_movCam.cols)), 1, fundamentalMat, lines);

				for (int j = 0; j < lines.size(); ++j) {
					float stCamKpPosX = usedKeypoints_stCam.at<float>(0, j);
					float stCamKpPosY = usedKeypoints_stCam.at<float>(1, j);
					float error = abs(lines.at(j)[0] * stCamKpPosX + lines.at(j)[1] * stCamKpPosY + lines.at(j)[2]) / (sqrtf(powf(lines.at(j)[0], 2.0f) + powf(lines.at(j)[1], 2.0f)));
				
					if (threshold < error)
						++outlierCount;
				}
			}

			if (outlierCount <= minOutlierCount) {
				*selectedCam1 = cam1;
				*selectedCam2 = cam2;
				minOutlierCount = outlierCount;
				minOutlierExtParamIndex = extParamCount;
			}
			cout << "cam:" << cam1 << ", " << cam2 << "-> outlierCount: " << outlierCount << endl;

			++extParamCount;
		}
	}

	return extParamCandidates.at(minOutlierExtParamIndex);
}

void Method::plotPoints(cv::Mat argInputImage, vector< cv::Point3f > argPoints3D, cv::Mat argRt, cv::Mat argIParam, cv::Mat argDistCoeffs, int frameIdx)
{
	vector< cv::Point2f > points2D;
	cv::Mat rVec;
	cv::Mat tVec;

	cv::Rodrigues(argRt(cv::Rect(0, 0, 3, 3)), rVec);
	tVec = argRt(cv::Rect(3, 0, 1, 3));
	cv::projectPoints(argPoints3D, rVec, tVec, argIParam, argDistCoeffs, points2D);

	ofstream ofs;
	ofs.open("./reprojectionResult/reproj_frame" + to_string(frameIdx) + ".csv");
	cv::Mat tmpInputImage = argInputImage.clone();
	for (int i = 0; i < points2D.size(); ++i) {
		cv::circle(tmpInputImage, points2D.at(i), 2, cv::Scalar(0, 255, 0), 2);
		cv::putText(tmpInputImage, to_string(i), points2D.at(i), CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 255, 0));

		ofs << points2D.at(i).x << ',' << points2D.at(i).y << endl;
	}
	ofs.close();

	stringstream ssFileXML("");
	cv::Mat outMat(points2D.size(), 2, CV_32F);
	for (int i = 0; i < points2D.size(); ++i)
	{
		outMat.at<float>(i, 0) = points2D.at(i).x;
		outMat.at<float>(i, 1) = points2D.at(i).y;
	}
	ssFileXML << "./reprojectionResult/reproj_" + to_string(frameIdx) + ".xml";
	cv::FileStorage cvfs(ssFileXML.str(), CV_STORAGE_WRITE);
	cvfs << "projectedPts" << outMat;

	stringstream ssWin("");
	ssWin << "reprojection" + to_string(frameIdx);
	cv::imshow(ssWin.str(), tmpInputImage);
	cv::waitKey(1);

	stringstream ssFile("");
	ssFile << "./reprojectionResultImage/" << ssWin.str() << ".png";
	cv::imwrite(ssFile.str(), tmpInputImage);

	return;
}

//void Method::projectImage(cv::Mat argMovCamImage, cv::Mat argTexImage, std::vector< cv::Point3f > argPoints3D, cv::Mat argRt, cv::Mat argIParam, cv::Mat argDistCoeffs)
//{
//	vector< cv::Point2f > points2D;
//	cv::Mat rVec;
//	cv::Mat tVec;
//
//	cv::Rodrigues(argRt(cv::Rect(0, 0, 3, 3)), rVec);
//	tVec = argRt(cv::Rect(3, 0, 1, 3));
//	cv::projectPoints(argPoints3D, rVec, tVec, argIParam, argDistCoeffs, points2D);
//
//	cv::Mat texImage = argTexImage.clone();
//
//	std::vector< cv::Point2f > texImgCorner(4);
//	texImgCorner.at(0) = cv::Point2f(0.0f, 0.0f);
//	texImgCorner.at(1) = cv::Point2f(texImage.cols, 0.0f);
//	texImgCorner.at(2) = cv::Point2f(texImage.cols, texImage.rows);
//	texImgCorner.at(3) = cv::Point2f(0.0f, texImage.rows);
//
//	cv::Mat HMat = cv::findHomography(texImgCorner, points2D);
//	//cout << "HMat:" << endl << HMat << endl;
//
//	cv::Mat tmpSrcPoint(3, 1, CV_64F);
//	cv::Mat tmpDstPoint;
//	for (int row = 0; row < texImage.rows; ++row) {
//		for (int col = 0; col < texImage.cols; ++col) {
//			tmpSrcPoint.at<double>(0, 0) = static_cast<double>(col);
//			tmpSrcPoint.at<double>(1, 0) = static_cast<double>(row);
//			tmpSrcPoint.at<double>(2, 0) = 1.0f;
//			tmpDstPoint = HMat * tmpSrcPoint;
//			int dstPosX = static_cast<int>(tmpDstPoint.at<double>(0, 0) / tmpDstPoint.at<double>(2, 0));
//			int dstPosY = static_cast<int>(tmpDstPoint.at<double>(1, 0) / tmpDstPoint.at<double>(2, 0));
//			if ((0 <= dstPosX && dstPosX < argMovCamImage.cols) && (0 <= dstPosY && dstPosY < argMovCamImage.rows)) {
//				argMovCamImage.at<cv::Vec3b>(dstPosY, dstPosX) = texImage.at<cv::Vec3b>(row, col);
//			}
//		}
//	}
//
//	return;
//}