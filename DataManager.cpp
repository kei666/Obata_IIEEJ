#include "DataManager.h"

DataManager::DataManager()
{

}

DataManager::~DataManager()
{

}

void DataManager::readConfig()
{
	// ========================== //
	/* ----- Initialization ----- */
	// ========================== //
	// ----- Read config file
	ConfigReader config;
	// Inputs
	
	config.read("Inputs.mode", mode);
	config.read("Inputs.pathToStCamImgSeq", pathToStCamImgSeq);	//--
	config.read("Inputs.fileNameStCamPoses", fileNameStCamPoses);
	int wndWidth, wndHeight;
	config.read("Inputs.wndWidth", wndWidth);	// Window width
	config.read("Inputs.wndHeight", wndHeight);	// Window height

	config.read("Inputs.pathToMovCamImgSeq", pathToMovCamImgSeq);
	config.read("Inputs.fileNameMovCamImgSeq", fileNameMovCamImgSeq);
	config.read("Inputs.fileNameMovCamIntParams", fileNameMovCamIntParams);

	config.read("Inputs.imageFeature", imageFeature);

	config.read("Inputs.pathToTexture", pathToTexture);
	config.read("Inputs.fileNameTexture1", fileNameTexture1);
	config.read("Inputs.fileNameTexture2", fileNameTexture2);
	config.read("Inputs.fileNameTexture3", fileNameTexture3);
	config.read("Inputs.fileNameTextureCorners", fileNameTextureCorners);

	config.read("Inputs.pathToExpData", pathToExpData);
	config.read("Inputs.fileNameReproj3DPts", fileNameReproj3DPts);
	//config.read("Inputs.fileNameGroundTruth", fileNameGroundTruth);

	// ----- Read PhotoScan data(xml, Rotation)
	::PhotoScanCameraXMLReader(pathToStCamImgSeq + fileNameStCamPoses, psCameras);
}



void DataManager::readStCamImgSeq(
	std::vector< cv::Mat > &argImgs,
	std::vector< cv::Mat > &argExtParams)
{

	argImgs.resize(psCameras.matrices.size());
	argExtParams.resize(psCameras.matrices.size());

	//featureDetectorAndComputer = cv::AKAZE::create();	//--?

	for (int i = 0; i < psCameras.matrices.size(); ++i) {
		argImgs.at(i) = cv::imread(pathToStCamImgSeq + "cap[" + to_string(i) + "].png", 1);
		argExtParams.at(i) = (psCameras.matrices.at(i)).inv();
	}
}

void DataManager::readStCamImgSeq(std::vector<std::vector<cv::Mat> > &argImgs, std::vector<cv::Mat> &argExtParams) {
	argImgs.resize(psCameras.matrices.size());
	argExtParams.resize(psCameras.matrices.size());

	//featureDetectorAndComputer = cv::AKAZE::create();	//--?

	for (int i = 0; i < psCameras.matrices.size(); ++i) {
		cv::VideoCapture cap;
		cap.open(pathToStCamImgSeq + "cap[" + to_string(i) + "].avi");
		int movCamImgSeqSize = static_cast<int>(cap.get(CV_CAP_PROP_FRAME_COUNT));
		argImgs.at(i).resize(movCamImgSeqSize);
		for (int j = 0; j < movCamImgSeqSize; ++j) {
			cap >> argImgs.at(i).at(j);
		}
		argExtParams.at(i) = (psCameras.matrices.at(i)).inv();
	}
}

void DataManager::readMovCamImgSeq(std::vector< cv::Mat > &argImgs)
{
	cv::VideoCapture cap;
	cap.open(pathToMovCamImgSeq + fileNameMovCamImgSeq);
	int movCamImgSeqSize = static_cast<int>(cap.get(CV_CAP_PROP_FRAME_COUNT));

	argImgs.resize(movCamImgSeqSize);

	for (int i = 0; i < movCamImgSeqSize; ++i) {	
		cap >> argImgs.at(i);
	}
}

void DataManager::readIntrinsicsStCam(cv::Mat &argIntParams, cv::Mat &argDistCoeffs)
{
		argIntParams = cv::Mat::eye(3, 3, CV_32F);
		argIntParams.at<float>(0, 0) = static_cast<float>(psCameras.sensorData.at(0).fx);
		argIntParams.at<float>(1, 1) = static_cast<float>(psCameras.sensorData.at(0).fy);
		argIntParams.at<float>(0, 2) = static_cast<float>(psCameras.sensorData.at(0).cx);
		argIntParams.at<float>(1, 2) = static_cast<float>(psCameras.sensorData.at(0).cy);
		argDistCoeffs.create(1, 5, CV_32F);
		argDistCoeffs.at<float>(0, 0) = static_cast<float>(psCameras.sensorData.at(0).k1);
		argDistCoeffs.at<float>(0, 1) = static_cast<float>(psCameras.sensorData.at(0).k2);
		argDistCoeffs.at<float>(0, 2) = static_cast<float>(psCameras.sensorData.at(0).p1);
		argDistCoeffs.at<float>(0, 3) = static_cast<float>(psCameras.sensorData.at(0).p2);
		argDistCoeffs.at<float>(0, 4) = static_cast<float>(psCameras.sensorData.at(0).k3);
}

void DataManager::readIntrinsicsMovCam(cv::Mat &argIntParams, cv::Mat &argDistCoeffs)
{
	cv::FileStorage cvfs(pathToMovCamImgSeq + fileNameMovCamIntParams, CV_STORAGE_READ);
	cv::FileNode node(cvfs.fs, NULL);
	cv::read(node["intrinsic"], argIntParams);
	cv::read(node["distCoeffs"], argDistCoeffs);
	argIntParams.convertTo(argIntParams, CV_32F);
	argDistCoeffs.convertTo(argDistCoeffs, CV_32F);
}

void DataManager::readTextureImage(std::vector< cv::Mat > &argTexImgs, std::vector< std::vector< cv::Point3f > > &argTexImgCorners)
{
	cv::Mat tmpTexuteImage = cv::imread(pathToTexture + fileNameTexture1, 1);
	argTexImgs.push_back(tmpTexuteImage);
	tmpTexuteImage = cv::imread(pathToTexture + fileNameTexture2, 1);
	argTexImgs.push_back(tmpTexuteImage);
	tmpTexuteImage = cv::imread(pathToTexture + fileNameTexture3, 1);
	argTexImgs.push_back(tmpTexuteImage);

	//--�R�[�i�[3�����ʒu�ǂݍ���
	cv::FileStorage cvfs(pathToTexture + fileNameTextureCorners, CV_STORAGE_READ);
	cv::FileNode node(cvfs.fs, NULL);
	int textureNum = static_cast<int>(cvfs["pointNum"]) / 4;
	argTexImgCorners.resize(textureNum);
	for (int i = 0; i < static_cast<int>(cvfs["pointNum"]); ++i) {

		int textureID = i / 4;

		cv::Mat pt3D_Mat;
		cv::read(node["pt3D_" + to_string(i)], pt3D_Mat);

		cv::Point3f tmpPt3D;
		tmpPt3D.x = pt3D_Mat.at<float>(0, 0);
		tmpPt3D.y = pt3D_Mat.at<float>(1, 0);
		tmpPt3D.z = pt3D_Mat.at<float>(2, 0);

		argTexImgCorners.at(textureID).push_back(tmpPt3D);
	}
}


void DataManager::readExpData(std::vector< cv::Point3f > &argPts3D, std::vector< cv::Point2f > &argPts2D)
{
	cv::FileStorage cvfs(pathToExpData + fileNameReproj3DPts, CV_STORAGE_READ);
	cv::FileNode node(cvfs.fs, NULL);
	argPts3D.resize(static_cast<int>(cvfs["pointNum"]));
	for (int i = 0; i < argPts3D.size(); ++i) {
		cv::Mat pt3D_Mat;
		cv::read(node["pt3D_" + to_string(i)], pt3D_Mat);
		argPts3D.at(i).x = pt3D_Mat.at<float>(0, 0);
		argPts3D.at(i).y = pt3D_Mat.at<float>(1, 0);
		argPts3D.at(i).z = pt3D_Mat.at<float>(2, 0);
	}

	//--�^�l��2�������W�ǂݍ���


}