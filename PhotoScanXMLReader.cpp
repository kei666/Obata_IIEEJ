#include "./PhotoScanXMLReader.h"

void PhotoScanCameraXMLReader(const string path, PhotoScanCameras &data)
{
	namespace xml = boost::property_tree;

	// ----- Read XML file
	xml::ptree pt;
	xml::read_xml(path, pt);
	cout << "Reading " << path << "..." << endl;

	// ----- Sensors
	cout << "Sensor Data" << endl;
	BOOST_FOREACH(const xml::ptree::value_type &child, pt.get_child("document.chunk.sensors"))
	{
		PhotoScanSensorData tmpSensorData;

		cout << "Sensor ID: " << child.second.get_optional<int>("<xmlattr>.id") << endl;

		// Width and height from "resolution" attribute
		boost::optional<int> width = child.second.get_optional<int>("calibration.resolution.<xmlattr>.width");
		boost::optional<int> height = child.second.get_optional<int>("calibration.resolution.<xmlattr>.height");
		tmpSensorData.width = width.get();
		tmpSensorData.height = height.get();
		cout << "  width: " << width.get() << ", height: " << height.get() << endl;

		// Other intrinsic parameters
		tmpSensorData.fx	= child.second.get<double>("calibration.fx");
		tmpSensorData.fy	= child.second.get<double>("calibration.fy");
		cout << "  fx: " << tmpSensorData.fx << ", fy: " << tmpSensorData.fy << endl;
		tmpSensorData.cx	= child.second.get<double>("calibration.cx");
		tmpSensorData.cy	= child.second.get<double>("calibration.cy");
		cout << "  cx: " << tmpSensorData.cx << ", cy: " << tmpSensorData.cy << endl;
		////tmpSensorData.skew	= child.second.get<double>("calibration.skew");
		////cout << "  skew: " << tmpSensorData.skew << endl;
		tmpSensorData.k1	= child.second.get<double>("calibration.k1");
		tmpSensorData.k2	= child.second.get<double>("calibration.k2");
		tmpSensorData.k3	= child.second.get<double>("calibration.k3");
		cout << "  k1: " << tmpSensorData.k1 << ", k2: " << tmpSensorData.k2 << ", k3: " << tmpSensorData.k3 << endl;
		tmpSensorData.p1	= child.second.get<double>("calibration.p1");
		tmpSensorData.p2	= child.second.get<double>("calibration.p2");
		cout << "  p1: " << tmpSensorData.p1 << ", p2: " << tmpSensorData.p2 << endl;

		// Push back the data
		data.sensorData.push_back(tmpSensorData);
	}

	// ----- Cameras
	cout << "Loaded camera IDs: ";
	BOOST_FOREACH(const xml::ptree::value_type &child, pt.get_child("document.chunk.cameras"))
	{
		//glm::mat4 tmpMat[2];
		list<string> strList;
		boost::optional<string> label = child.second.get_optional<string>("<xmlattr>.label");
		
		cout << child.second.get_optional<int>("<xmlattr>.id") << " (" << label.get() << ") " << endl;
		data.imgNames.push_back(label.get());

		// Get string
		string str = child.second.get<string>("transform");
		// and split it
		boost::split(strList, str, boost::is_space());

		/*
		int idx = 0;
		BOOST_FOREACH(string s, strList) {
			stringstream convert(s);
			convert >> tmpMat[1][idx % 4][idx / 4];
			++idx;
		}
		
		// Swap coordinates from "x->right, y->down, z->front" to "x->right, y->up, z-back"
		tmpMat[0][0] = tmpMat[1][0];	// x->right to x->right
		tmpMat[0][1] = -tmpMat[1][1];	// y->down to y->up
		tmpMat[0][2] = -tmpMat[1][2];	// z->front to z->back
		tmpMat[0][3] = tmpMat[1][3];

		data.matrices.push_back(glm::inverse(tmpMat[0]));
		*/

		int idx = 0;
		cv::Mat tmpPose(4, 4, CV_32F);
		BOOST_FOREACH(string s, strList) {
			stringstream convert(s);

			convert >> tmpPose.at<float>(idx / 4, idx % 4);
			++idx;

		}

		data.matrices.push_back(tmpPose);
		//cout << tmpPose << endl;
	}
	cout << endl;
}

void PhotoScanCameraXMLWriter(const string path, cv::Mat iParam, cv::Mat distCoeffs, std::vector<cv::Mat> camPoses)
{
	using boost::property_tree::ptree;
	int i = 0;

	ptree pt;
	pt.add("document.<xmlattr>.version", "1.2.0");
	BOOST_FOREACH(const cv::Mat camPose, camPoses) {
		ptree& child = pt.add("document.chunk.cameras", "");
		child.put("<xmlattr>.label", "aaa");
		child.put("<xmlattr>.id", "iii");
		child.put("<xmlattr>.enabled", "uuu");
		child.put("<xmlattr>.sensor_id", "eee");

		//pt.put("", camPoses.at(i).data);

		++i;
	}

	using namespace boost::property_tree::xml_parser;
	const int indent = 2;
	//write_xml(path, pt, std::locale(), xml_writer_make_settings(' ', indent, widen<char>("utf-8")));
	write_xml(path, pt, std::locale(), xml_writer_make_settings<std::string>(' ', indent));

}